package data;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MatchData {
    @CsvBindByPosition(position = 0)
    private UUID id;
    @CsvBindByPosition(position = 1)
    private double rateA;
    @CsvBindByPosition(position = 2)
    private double rateB;
    @CsvBindByPosition(position = 3)
    private Side result;

}
