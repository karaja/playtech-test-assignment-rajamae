package file;

import data.MatchData;
import data.Operation;
import data.PlayerData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DataValidator {
    private static final Logger LOGGER = LoggerFactory.getLogger(DataValidator.class);

    public boolean isValidData(List<PlayerData> playerData, List<MatchData> matchData) {
        return isPlayerDataValid(playerData) && isMatchDataValid(matchData);
    }

    private boolean isPlayerDataValid(List<PlayerData> playerData) {
        for (PlayerData data : playerData) {
            if (data.getId() == null) {
                LOGGER.error("PlayerData object with missing ID found.");
                return false;
            }

            if (data.getOperation() == Operation.BET && data.getMatchId() == null) {
                LOGGER.error("PlayerData object with missing match ID found.");
                return false;
            }
        }
        return true;
    }

    private boolean isMatchDataValid(List<MatchData> matchData) {
        for (MatchData data : matchData) {
            if (data.getId() == null) {
                LOGGER.error("MatchData object with missing ID found.");
                return false;
            }
        }
        return true;
    }
}
