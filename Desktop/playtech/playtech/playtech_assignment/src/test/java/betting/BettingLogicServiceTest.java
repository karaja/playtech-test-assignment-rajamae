package betting;

import data.MatchData;
import data.Operation;
import data.Player;
import data.PlayerData;
import data.Side;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class BettingLogicServiceTest {

    @Test
    void getIncrementShouldReturnZeroForNegativeOutcome() {
        assertEquals(BigDecimal.ZERO, BettingLogicService.getIncrement(-1));
    }

    @Test
    void getIncrementShouldReturnOneForPositiveOutcome() {
        assertEquals(BigDecimal.ONE, BettingLogicService.getIncrement(1));
    }

    @Test
    void getIncrementShouldReturnZeroForZeroOutcome() {
        assertEquals(BigDecimal.ZERO, BettingLogicService.getIncrement(0));
    }

    @Test
    void getWinnerSideShouldReturnCorrectWinner() {
        UUID matchId = UUID.randomUUID();
        List<MatchData> matchData = List.of(
                new MatchData(matchId, 1.5, 0.8, Side.A),
                new MatchData(UUID.randomUUID(), 2.0, 0.5, Side.B),
                new MatchData(UUID.randomUUID(), 0.7, 1.2, Side.DRAW)
        );

        assertEquals(Side.A, BettingLogicService.getWinnerSide(matchData, matchId));
    }

    @Test
    void getWinnerSideShouldThrowExceptionForUnknownMatchId() {
        UUID matchId = UUID.randomUUID();
        List<MatchData> matchData = List.of(
                new MatchData(UUID.randomUUID(), 1.5, 0.8, Side.A),
                new MatchData(UUID.randomUUID(), 2.0, 0.5, Side.B),
                new MatchData(UUID.randomUUID(), 0.7, 1.2, Side.DRAW)
        );

        assertThrows(RuntimeException.class, () -> BettingLogicService.getWinnerSide(matchData, matchId));
    }

    @Test
    void getMatchRatesShouldReturnMapOfMatchRates() {
        List<MatchData> matchData = List.of(
                new MatchData(UUID.randomUUID(), 1.5, 0.8, Side.A),
                new MatchData(UUID.randomUUID(), 2.0, 0.5, Side.B),
                new MatchData(UUID.randomUUID(), 0.7, 1.2, Side.DRAW)
        );

        var expectedRates = Map.of(
                matchData.get(0).getId(), List.of(1.5, 0.8),
                matchData.get(1).getId(), List.of(2.0, 0.5),
                matchData.get(2).getId(), List.of(0.7, 1.2)
        );

        assertEquals(expectedRates, BettingLogicService.getMatchRates(matchData));
    }

    @Test
    void calculateBetOutcomeShouldReturnCorrectOutcomeForWinningBet() {
        PlayerData playerData = new PlayerData(UUID.randomUUID(), Operation.BET, UUID.randomUUID(), 100, Side.A);
        Player player = new Player(UUID.randomUUID(), 200, 0, BigDecimal.ZERO, true, new ArrayList<>(), 0);

        int outcome = BettingLogicService.calculateBetOutcome(playerData, 1.5, Side.A);

        assertEquals(150, outcome);
    }

    @Test
    void calculateBetOutcomeShouldReturnZeroForDraw() {
        PlayerData playerData = new PlayerData(UUID.randomUUID(), Operation.BET, UUID.randomUUID(), 100, Side.A);
        Player player = new Player(UUID.randomUUID(), 200, 0, BigDecimal.ZERO, true, new ArrayList<>(), 0);

        int outcome = BettingLogicService.calculateBetOutcome(playerData, 1.5, Side.DRAW);

        assertEquals(0, outcome);
    }

    @Test
    void calculateBetOutcomeShouldReturnNegativeOutcomeForLosingBet() {
        PlayerData playerData = new PlayerData(UUID.randomUUID(), Operation.BET, UUID.randomUUID(), 100, Side.A);
        Player player = new Player(UUID.randomUUID(), 50, 0, BigDecimal.ZERO, true, new ArrayList<>(), 0);

        int outcome = BettingLogicService.calculateBetOutcome(playerData, 1.5, Side.B);

        assertEquals(-100, outcome);
    }

    @Test
    void getCasinoHostBalanceShouldReturnCorrectBalance() {
        List<Player> players = List.of(
                new Player(UUID.randomUUID(), 100, 0, BigDecimal.ZERO, true, new ArrayList<>(), 50),
                new Player(UUID.randomUUID(), 200, 0, BigDecimal.ZERO, true, new ArrayList<>(), -30),
                new Player(UUID.randomUUID(), 50, 0, BigDecimal.ZERO, false, new ArrayList<>(), 10)
        );

        assertEquals(20, BettingLogicService.getCasinoHostBalance(players));
    }
}
