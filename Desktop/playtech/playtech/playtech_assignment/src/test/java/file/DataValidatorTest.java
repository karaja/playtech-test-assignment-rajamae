package file;
import data.MatchData;
import data.Operation;
import data.PlayerData;
import data.Side;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DataValidatorTest {

    @Test
    public void isValidData_withValidPlayerAndMatchData_shouldReturnTrue() {
        DataValidator dataValidator = new DataValidator();

        List<PlayerData> validPlayerData = new ArrayList<>();
        validPlayerData.add(new PlayerData(UUID.randomUUID(), Operation.DEPOSIT, null, 100, Side.A));

        List<MatchData> validMatchData = new ArrayList<>();
        validMatchData.add(new MatchData(UUID.randomUUID(), 1.5, 2.0, Side.A));

        assertTrue(dataValidator.isValidData(validPlayerData, validMatchData));
    }

    @Test
    public void isValidData_withMissingPlayerId_shouldReturnFalse() {
        DataValidator dataValidator = new DataValidator();

        List<PlayerData> invalidPlayerData = new ArrayList<>();
        invalidPlayerData.add(new PlayerData(null, Operation.DEPOSIT, null, 100, Side.A));

        List<MatchData> validMatchData = new ArrayList<>();
        validMatchData.add(new MatchData(UUID.randomUUID(), 1.5, 2.0, Side.A));

        assertFalse(dataValidator.isValidData(invalidPlayerData, validMatchData));
    }

    @Test
    public void isValidData_withMissingMatchIdForBetOperation_shouldReturnFalse() {
        DataValidator dataValidator = new DataValidator();

        List<PlayerData> invalidPlayerData = new ArrayList<>();
        invalidPlayerData.add(new PlayerData(UUID.randomUUID(), Operation.BET, null, 100, Side.A));

        List<MatchData> validMatchData = new ArrayList<>();
        validMatchData.add(new MatchData(UUID.randomUUID(), 1.5, 2.0, Side.A));

        assertFalse(dataValidator.isValidData(invalidPlayerData, validMatchData));
    }

    @Test
    public void isValidData_withMissingMatchIdForOtherOperations_shouldReturnTrue() {
        DataValidator dataValidator = new DataValidator();

        List<PlayerData> validPlayerData = new ArrayList<>();
        validPlayerData.add(new PlayerData(UUID.randomUUID(), Operation.DEPOSIT, null, 100, Side.A));

        List<MatchData> validMatchData = new ArrayList<>();
        validMatchData.add(new MatchData(UUID.randomUUID(), 1.5, 2.0, Side.A));

        assertTrue(dataValidator.isValidData(validPlayerData, validMatchData));
    }

}
