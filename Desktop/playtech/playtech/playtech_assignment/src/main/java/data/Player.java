package data;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Player {
    private UUID id;
    private int balance;
    private int movesCount;
    private BigDecimal winCount;
    private boolean isLegitimate;
    private List<PlayerData> illegalMoves;
    private int individualCasinoHostBalanceChange;

}
