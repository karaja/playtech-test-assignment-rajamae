package betting;

import betting.BettingService;
import data.*;
import org.junit.jupiter.api.Test;
import player.PlayerProcessor;
import player.PlayerStatisticsService;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class BettingServiceTest {

    @Test
    public void testProcessBettingData() {
        BettingService bettingService = new BettingService();

        UUID match = UUID.randomUUID();
        UUID player = UUID.randomUUID();

        List<PlayerData> playerData = Arrays.asList(
                createPlayerData(player, Operation.DEPOSIT, null, 1000, null),
                createPlayerData(player, Operation.BET, match, 200, Side.A),
                createPlayerData(player, Operation.WITHDRAW, null, 100, null)
        );

        List<MatchData> matchData = List.of(
                createMatchData(match, 1.5, 0.8, Side.A)
        );

        bettingService.processBettingData(playerData, matchData);
        Player data = PlayerProcessor.findPlayerById(player);

        assertEquals(1200, data.getBalance());
    }

    @Test
    public void testPerformDeposit() {
        PlayerData depositData = createPlayerData(UUID.randomUUID(), Operation.DEPOSIT, null, 500, null);
        Player player = PlayerProcessor.addPlayer(depositData);

        BettingService.performDeposit(depositData, player);

        assertEquals(500, player.getBalance());
    }

    @Test
    public void testPerformWithdraw() {
        PlayerData depositData = createPlayerData(UUID.randomUUID(), Operation.DEPOSIT, null, 500, null);
        PlayerData withdrawData = createPlayerData(UUID.randomUUID(), Operation.WITHDRAW, null, 100, null);
        Player player = PlayerProcessor.addPlayer(depositData);

        BettingService.performDeposit(depositData, player);
        BettingService.performWithdraw(withdrawData, player);

        assertEquals(400, player.getBalance());
    }

    private PlayerData createPlayerData(UUID id, Operation operation, UUID matchId, int amount, Side side) {
        return new PlayerDataBuilder()
                .withId(id)
                .withOperation(operation)
                .withMatchId(matchId)
                .withAmount(amount)
                .withSide(side)
                .build();
    }

    private MatchData createMatchData(UUID id, double rateA, double rateB, Side result) {
        return new MatchDataBuilder()
                .withId(id)
                .withRateA(rateA)
                .withRateB(rateB)
                .withResult(result)
                .build();
    }

    private static class PlayerDataBuilder {
        private UUID id;
        private Operation operation;
        private UUID matchId;
        private int amount;
        private Side side;

        public PlayerDataBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public PlayerDataBuilder withOperation(Operation operation) {
            this.operation = operation;
            return this;
        }

        public PlayerDataBuilder withMatchId(UUID matchId) {
            this.matchId = matchId;
            return this;
        }

        public PlayerDataBuilder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public PlayerDataBuilder withSide(Side side) {
            this.side = side;
            return this;
        }

        public PlayerData build() {
            return new PlayerData(id, operation, matchId, amount, side);
        }
    }

    private static class MatchDataBuilder {
        private UUID id;
        private double rateA;
        private double rateB;
        private Side result;

        public MatchDataBuilder withId(UUID id) {
            this.id = id;
            return this;
        }

        public MatchDataBuilder withRateA(double rateA) {
            this.rateA = rateA;
            return this;
        }

        public MatchDataBuilder withRateB(double rateB) {
            this.rateB = rateB;
            return this;
        }

        public MatchDataBuilder withResult(Side result) {
            this.result = result;
            return this;
        }

        public MatchData build() {
            return new MatchData(id, rateA, rateB, result);
        }
    }
}
