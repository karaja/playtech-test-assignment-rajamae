package betting;

import data.MatchData;
import data.Player;
import data.PlayerData;
import data.Side;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class BettingLogicService {

    protected static BigDecimal getIncrement(int outcome) {
        BigDecimal increment = new BigDecimal("0");
        if (outcome > 0) {
            increment = new BigDecimal("1");
        }
        return increment;
    }

    protected static Side getWinnerSide(List<MatchData> matchData, UUID matchId) {
        return matchData.stream()
                .filter(m -> m.getId().equals(matchId))
                .findFirst()
                .map(MatchData::getResult)
                .orElseThrow(() -> new RuntimeException("No winner side specified"));
    }

    protected static Map<UUID, List<Double>> getMatchRates(List<MatchData> matchData) {
        Map<UUID, List<Double>> matchRates = new HashMap<>();
        for (MatchData match : matchData) {
            matchRates.put(match.getId(), List.of(match.getRateA(), match.getRateB()));
        }
        return matchRates;
    }

    protected static int calculateBetOutcome(PlayerData player, double rate, Side winner) {
        if (player.getSide().equals(winner)) {
            return (int) (player.getAmount() * rate);
        } else if (winner.equals(Side.DRAW)) {
            return 0;
        } else {
            return -player.getAmount();
        }
    }

    public static int getCasinoHostBalance(List<Player> players) {
        int casinoHostBalance = 0;
        for (Player player : players) {
            if (player.isLegitimate()) {
                casinoHostBalance += player.getIndividualCasinoHostBalanceChange();
            }
        }
        return casinoHostBalance;
    }
}
