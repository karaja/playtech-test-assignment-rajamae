package file;
import data.Player;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerStatisticsService;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static betting.BettingLogicService.getCasinoHostBalance;

public class FileProcessor {
    private static final String RESULT_FILENAME = "src/main/java/betting/result.txt";
    private static final Logger LOGGER = LoggerFactory.getLogger(DataValidator.class);

    private final PlayerStatisticsService playerStatisticsService;

    public FileProcessor(PlayerStatisticsService playerStatisticsService) {
        this.playerStatisticsService = playerStatisticsService;
    }
    public void writeToFile(List<Player> players) {
        try (PrintWriter writer = new PrintWriter(RESULT_FILENAME)) {
            List<String> legitimate = playerStatisticsService.getLegitimatePlayersStatistics(players);
            for (String s : legitimate) {
                writer.println(s);
            }
            writer.println();

            List<String> illegitimate = playerStatisticsService.getIllegitimatePlayersStatistics(players);

            for (String s : illegitimate) {
                writer.println(s);
            }
            writer.println();

            writer.println(getCasinoHostBalance(players));

        } catch (IOException e) {
            LOGGER.error("An error occurred while writing to the file.", e);
            throw new RuntimeException("An error occurred while writing to the file.", e);
        }
    }
}