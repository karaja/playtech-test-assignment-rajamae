package player;

import data.Player;
import data.PlayerData;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

import static betting.BettingProcessor.players;

public class PlayerProcessor {

    public static Player addPlayer(PlayerData player) {
        Player newPlayer;
        if (players.stream().noneMatch(p -> Objects.equals(p.getId(), player.getId()))) {
            newPlayer = Player.builder()
                    .id(player.getId())
                    .balance(0)
                    .movesCount(0)
                    .winCount(new BigDecimal(0))
                    .isLegitimate(true)
                    .illegalMoves(new ArrayList<>())
                    .build();
            players.add(newPlayer);
        } else {
            newPlayer = findPlayerById(player.getId());
        }
        return newPlayer;
    }
    public static Player findPlayerById(UUID id) {
        return players.stream()
                .filter(player -> id != null && id.equals(player.getId()))
                .findFirst()
                .orElse(null);
    }

    public static void setIllegitimate(PlayerData playerDataObject, Player player) {
        player.setLegitimate(false);
        List<PlayerData> moves = player.getIllegalMoves();
        if (moves == null) {
            moves = new ArrayList<>();
        }
        moves.add(playerDataObject);
        player.setIllegalMoves(moves);
    }
}
