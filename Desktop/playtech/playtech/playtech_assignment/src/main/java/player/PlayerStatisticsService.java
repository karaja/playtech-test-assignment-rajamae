package player;

import data.Player;
import data.PlayerData;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class PlayerStatisticsService {

    public List<String> getLegitimatePlayersStatistics(List<Player> players) {
        return players.stream()
                .filter(Player::isLegitimate)
                .sorted(Comparator.comparing(Player::getId))
                .map(this::formatLegitimatePlayerStatistics)
                .collect(Collectors.toList());
    }

    public List<String> getIllegitimatePlayersStatistics(List<Player> players) {
        return players.stream()
                .filter(player -> !player.isLegitimate())
                .sorted(Comparator.comparing(Player::getId))
                .map(this::formatIllegitimatePlayerStatistics)
                .collect(Collectors.toList());
    }

    private String formatLegitimatePlayerStatistics(Player player) {
        BigDecimal winCount = player.getWinCount();
        BigDecimal movesCount = BigDecimal.valueOf(player.getMovesCount());

        BigDecimal ratio = movesCount.equals(BigDecimal.ZERO) ?
                BigDecimal.ZERO :
                winCount.divide(movesCount, 2, RoundingMode.HALF_UP);

        return String.format("%s %d %.2f",
                player.getId(), player.getBalance(), ratio);
    }

    private String formatIllegitimatePlayerStatistics(Player player) {
        PlayerData firstIllegalMove = player.getIllegalMoves().stream().findFirst().orElse(null);
        if (Objects.isNull(firstIllegalMove)) return "";
        return String.format("%s %s %s %d %s",
                firstIllegalMove.getId(),
                firstIllegalMove.getOperation(),
                firstIllegalMove.getMatchId(),
                firstIllegalMove.getAmount(),
                firstIllegalMove.getSide());
    }
}
