package player;

import data.Operation;
import data.Player;
import data.PlayerData;
import data.Side;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerProcessorTest {

    @Test
    public void addPlayer_withNonExistingPlayer_shouldAddNewPlayer() {
        PlayerData playerData = createPlayerData();

        Player newPlayer = PlayerProcessor.addPlayer(playerData);

        assertNotNull(newPlayer);
        assertEquals(playerData.getId(), newPlayer.getId());
        assertEquals(0, newPlayer.getBalance());
        assertEquals(0, newPlayer.getMovesCount());
        assertEquals(BigDecimal.ZERO, newPlayer.getWinCount());
        assertTrue(newPlayer.isLegitimate());
        assertTrue(newPlayer.getIllegalMoves().isEmpty());
    }

    @Test
    public void addPlayer_withExistingPlayer_shouldReturnExistingPlayer() {
        PlayerData playerData = createPlayerData();
        List<Player> players = new ArrayList<>();
        Player existingPlayer = Player.builder()
                .id(playerData.getId())
                .winCount(BigDecimal.ZERO)
                .isLegitimate(true)
                .illegalMoves(new ArrayList<>())
                .build();
        players.add(existingPlayer);

        Player returnedPlayer = PlayerProcessor.addPlayer(playerData);

        assertNotNull(returnedPlayer);
        assertEquals(existingPlayer, returnedPlayer);
        assertTrue(players.contains(existingPlayer));
        assertEquals(1, players.size());
    }

    @Test
    public void findPlayerById_withNonExistingPlayer_shouldReturnNull() {
        UUID playerId = UUID.randomUUID();
        List<Player> players = new ArrayList<>();

        Player foundPlayer = PlayerProcessor.findPlayerById(playerId);

        assertNull(foundPlayer);
    }

    @Test
    public void setIllegitimate_withValidPlayerData_shouldSetPlayerAsIllegitimate() {
        PlayerData playerData = createPlayerData();
        Player player = Player.builder().id(playerData.getId()).build();

        PlayerProcessor.setIllegitimate(playerData, player);

        assertFalse(player.isLegitimate());
        assertEquals(1, player.getIllegalMoves().size());
        assertEquals(playerData, player.getIllegalMoves().get(0));
    }

    private PlayerData createPlayerData() {
        return PlayerData.builder()
                .id(UUID.randomUUID())
                .operation(Operation.DEPOSIT)
                .matchId(null)
                .amount(100)
                .side(Side.A)
                .build();
    }
}
