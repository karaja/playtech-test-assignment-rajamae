package betting;

import data.MatchData;
import data.Player;
import data.PlayerData;
import file.DataValidator;
import file.FileProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import player.PlayerStatisticsService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class BettingProcessor {
    private static final Logger LOGGER = LoggerFactory.getLogger(BettingProcessor.class);
    private static final String PLAYER_DATA_FILENAME = "src/main/resources/player_data.txt";
    private static final String MATCH_DATA_FILENAME = "src/main/resources/match_data.txt";
    public static final List<Player> players = new ArrayList<>();

    public static void main(String[] args) throws IOException {
        BettingService bettingService = new BettingService();
        DataValidator dataValidator = new DataValidator();
        PlayerStatisticsService playerStatisticsService = new PlayerStatisticsService();
        FileProcessor fileProcessor = new FileProcessor(playerStatisticsService);

        List<PlayerData> playerData = bettingService.readCsvData(PLAYER_DATA_FILENAME, PlayerData.class);
        List<MatchData> matchData = bettingService.readCsvData(MATCH_DATA_FILENAME, MatchData.class);

        if (dataValidator.isValidData(playerData, matchData)) {
            bettingService.processBettingData(playerData, matchData);
            fileProcessor.writeToFile(players);
        } else {
            LOGGER.error("Provided data is incomplete or invalid.");
        }
    }
}
