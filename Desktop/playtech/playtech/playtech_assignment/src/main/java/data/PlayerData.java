package data;

import com.opencsv.bean.CsvBindByPosition;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PlayerData {
    @CsvBindByPosition(position = 0)
    private UUID id;
    @CsvBindByPosition(position = 1)
    private Operation operation;
    @CsvBindByPosition(position = 2)
    private UUID matchId;
    @CsvBindByPosition(position = 3)
    private int amount;
    @CsvBindByPosition(position = 4)
    private Side side;
}
