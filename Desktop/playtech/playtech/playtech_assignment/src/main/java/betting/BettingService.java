package betting;

import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import data.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static betting.BettingLogicService.*;
import static player.PlayerProcessor.addPlayer;
import static player.PlayerProcessor.setIllegitimate;

public class BettingService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BettingService.class);

    protected  <T> List<T> readCsvData(String filename, Class<T> type) throws IOException {
        try (FileReader reader = new FileReader(filename)) {
            CsvToBean<T> csvToBean = new CsvToBeanBuilder<T>(reader)
                    .withType(type)
                    .withIgnoreLeadingWhiteSpace(true)
                    .build();

            return csvToBean.parse();
        } catch (IOException e) {
            throw new IOException("Can't read data from file: " + filename, e);
        }
    }

    protected void processBettingData(List<PlayerData> playerData, List<MatchData> matchData) {
        for (PlayerData playerDataObject : playerData) {
            Player player = addPlayer(playerDataObject);

            try {
                switch (playerDataObject.getOperation()) {
                    case DEPOSIT -> performDeposit(playerDataObject, player);
                    case BET -> {
                        if (!player.isLegitimate()) {
                            LOGGER.error("Player is not legitimate.");
                        }
                        else if (playerDataObject.getSide() == Side.A || playerDataObject.getSide() == Side.B) {
                            performBet(matchData, playerDataObject, player);
                        }
                        else {
                            LOGGER.error("Invalid side value.");
                        }
                    }
                    case WITHDRAW -> performWithdraw(playerDataObject, player);
                }
            } catch (RuntimeException e) {
                throw new RuntimeException("Invalid operation specified for Player ID: " + player.getId());
            }
        }
    }

    protected static void performDeposit(PlayerData playerDataObject, Player player) {
        if (player == null) {
            player = addPlayer(playerDataObject);
        }
        if (player.isLegitimate()) {
            player.setBalance(player.getBalance() + playerDataObject.getAmount());
        }
        else LOGGER.error("Can not perform deposit. Player is not legitimate.");
    }

    protected static void performWithdraw(PlayerData playerDataObject, Player player) {
        int withdrawAmount = playerDataObject.getAmount();
        if (player == null) {
            player = addPlayer(playerDataObject);
        }
        if (withdrawAmount > player.getBalance() || !player.isLegitimate()) {
            setIllegitimate(playerDataObject, player);
            LOGGER.error("Can not perform withdraw. Player is not legitimate.");
        } else {
            player.setBalance(player.getBalance() - withdrawAmount);
        }
    }

    protected static void performBet(List<MatchData> matchData, PlayerData playerDataObject, Player player) {
        Map<UUID, List<Double>> matchRates = getMatchRates(matchData);

        Side winner = getWinnerSide(matchData, playerDataObject.getMatchId());

        double rate = playerDataObject.getSide() == Side.A ? matchRates.get(playerDataObject.getMatchId()).get(0) : matchRates.get(playerDataObject.getMatchId()).get(1);

        int currentBalance = player.getBalance();

        if (playerDataObject.getAmount() > currentBalance) {
            setIllegitimate(playerDataObject, player);
        } else {
            player.setMovesCount(player.getMovesCount() + 1);
            int outcome = calculateBetOutcome(playerDataObject, rate, winner);
            player.setBalance(player.getBalance() + outcome);

            BigDecimal increment = getIncrement(outcome);

            player.setWinCount(player.getWinCount().add(increment));
            player.setIndividualCasinoHostBalanceChange(player.getIndividualCasinoHostBalanceChange() - outcome);
        }
    }
}
