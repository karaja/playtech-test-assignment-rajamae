# Betting Data Processor
## Introduction
This Java program processes betting data involving multiple players and matches. The program reads input data from two files, "player_data.txt" and "match_data.txt," and produces results in a file named "result.txt." The operations are instantaneous, and all events are ordered by time, with older events coming first.

## Description
- Players can perform three types of operations: Bet, Deposit, and Withdraw.
- Matches have two sides, A and B, with three possible results: A side won, B side won, or a Draw.
- Each match has rate values for sides A and B.
- Player winnings or losses are calculated based on their bets and the match results.
- Each player and match have unique IDs.
- Operations are considered "illegal" if a player attempts to bet or withdraw more coins than they have.
- Input data is grouped by player UUID.
- Each line contains information about one action.

## Output Data
In "result.txt," there are three expected result groups:

#### 1. Legitimate Players:

- List of legitimate player IDs with their final balance and betting win rate.
Example: **163f23ed-e9a9-4e54-a5b1-4e1fc86f12f4 4321 0.80**
#### 2. Illegitimate Players:

- List of illegitimate players represented by their first illegal operation.
Example: **163f23ed-e9a9-4e54-a5b1-4e1fc86f12f4 BET abae2255-4255-4304-8589-737cdff61640 5000 A**
#### 3. Casino Host Balance:

- Coin changes in the casino host's balance.
Example: **1500**
If a result section is empty, an empty line is written in the output file for that section.

## Code Overview
The code includes classes such as BettingLogicService, BettingService, BettingProcessor, FileProcessor, DataValidator, PlayerStatisticsService, and PlayerProcessor to handle the logic, processing, and player statistics. The data classes (Player, PlayerData, MatchData, Side, Operation) represent the entities involved.

## How to Run
Run the BettingProcessor class, and the results will be generated in the "result.txt" file.

Feel free to reach out if you have any questions or need further clarification.

Email: katlin.rajamae1@gmail.com
