package player;

import data.Operation;
import data.Player;
import data.PlayerData;
import data.Side;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class PlayerStatisticsServiceTest {

    @Test
    public void getLegitimatePlayersStatistics_withLegitimatePlayers_shouldReturnCorrectStatistics() {
        UUID player1 = UUID.fromString("2b20e5bb-9a32-4d33-b304-a9c7000e6de9");
        UUID player2 = UUID.fromString("5cbed318-af08-4046-947e-8e08b9f199c6");
        List<Player> players = new ArrayList<>();
        players.add(createPlayer(player1, 100, 3, 4));
        players.add(createPlayer(player2, 200, 2, 4));
        PlayerStatisticsService playerStatisticsService = new PlayerStatisticsService();

        List<String> statistics = playerStatisticsService.getLegitimatePlayersStatistics(players);

        assertEquals(2, statistics.size());

        assertEquals(player1 + " 100 0.75", statistics.get(0));
        assertEquals(player2 + " 200 0.50", statistics.get(1));
    }

    @Test
    public void getIllegitimatePlayersStatistics_withIllegitimatePlayers_shouldReturnCorrectStatistics() {
        UUID player1 = UUID.fromString("5cbed318-af08-4046-947e-8e08b9f199c6");
        UUID player2 = UUID.fromString("2b20e5bb-9a32-4d33-b304-a9c7000e6de9");
        UUID match = UUID.fromString("4925ac98-833b-454b-9342-13ed3dfd3ccf");
        List<Player> players = new ArrayList<>();
        players.add(createIllegitimatePlayer(player1, "DEPOSIT", null, 100, null));
        players.add(createIllegitimatePlayer(player2, "BET", match, 50, "A"));
        PlayerStatisticsService playerStatisticsService = new PlayerStatisticsService();

        List<String> statistics = playerStatisticsService.getIllegitimatePlayersStatistics(players);

        assertEquals(2, statistics.size());

        assertEquals(player1 + " DEPOSIT null 100 null", statistics.get(0));
        assertEquals(player2 + " BET " + match + " 50 A", statistics.get(1));
    }

    private Player createPlayer(UUID id, int balance, int winCount, int movesCount) {
        return Player.builder()
                .id(id)
                .balance(balance)
                .winCount(BigDecimal.valueOf(winCount))
                .movesCount(movesCount)
                .isLegitimate(true)
                .build();
    }

    private Player createIllegitimatePlayer(UUID id, String operation, UUID matchId, int amount, String side) {
        PlayerData illegalMove = PlayerData.builder()
                .id(id)
                .operation(Operation.valueOf(operation))
                .matchId(matchId)
                .amount(amount)
                .side(side != null ? Side.valueOf(side) : null)
                .build();

        return Player.builder()
                .id(UUID.randomUUID())
                .isLegitimate(false)
                .illegalMoves(List.of(illegalMove))
                .build();
    }
}
